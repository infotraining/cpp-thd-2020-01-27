#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <future>
#include <tuple>
#include <random>
#include "thread_safe_queue.hpp"
#include "joining_thread.hpp"

using namespace std::literals;

void background_work(size_t id, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    std::this_thread::sleep_for(delay);

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

using Task = std::function<void()>;

class ThreadPool
{
    ThreadSafeQueue<Task> tasks_q_;
    std::vector<ext::joining_thread> threads_;

    static constexpr auto end_of_work = nullptr;

    void run()
    {
        Task task;

        while (true)
        {
            tasks_q_.pop(task);

            if (task == end_of_work) // poisoning pill
                return;

            task(); // exectuting task in workers thread
        }
    }
public:
    ThreadPool(size_t size) : threads_(size)
    {
        for(size_t i = 0; i < size; ++i)
            threads_[i] = ext::joining_thread([this] { run();} );
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            tasks_q_.push(end_of_work); // sending poisoning pill to working threads
    }

    template <typename Func>
    auto submit(Func task)
    {
        using ResultT = decltype(task());

        auto pt = std::make_shared<std::packaged_task<ResultT()>>(task);
        auto f_result = pt->get_future();
        tasks_q_.push([pt]{ (*pt)(); });

        return f_result;
    }
};

int calculate_square(int x)
{
    std::cout << "Starting calculations for " << x << std::endl;

    if (x % 7 == 0)
        throw std::runtime_error("ERROR#7");

    std::random_device rd;
    std::uniform_int_distribution<> rnd_gen(200, 2000);
    std::this_thread::sleep_for(std::chrono::milliseconds(rnd_gen(rd)));

    return x * x;
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    std::vector<std::tuple<int, std::future<int>>> f_squares;

    {
        ThreadPool thread_pool(std::thread::hardware_concurrency());

        thread_pool.submit([] { background_work(1, 500ms);});

        for(int i = 2; i < 200; ++i)
        {
            thread_pool.submit([i] { background_work(i, 250ms);});
        }

        for(int i = 1; i <= 32; ++i)
        {
            f_squares.emplace_back(i, thread_pool.submit([i] { return calculate_square(i); }));
        }

    } // destructor waits for all submitted jobs to finish

    for(auto& fs : f_squares)
    {
        try
        {
            std::cout << "square " << std::get<0>(fs) << " - " << std::get<1>(fs).get() << std::endl;
        }
        catch(const std::runtime_error& e)
        {
            std::cout << e.what() << std::endl;
        }
    }

    std::cout << "Main thread ends..." << std::endl;
}
