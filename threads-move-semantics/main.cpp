#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "joining_thread.hpp"

using namespace std::literals;

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

std::thread create_background_work()
{
    static int id = 100;

    const int current_id = ++id;
    const std::string message = "THREAD#" + std::to_string(current_id);
    std::chrono::milliseconds delay(current_id);

    return std::thread{background_work, current_id, message, delay};
}

namespace Simplified
{
    class joining_thread
    {
        std::thread& thd_;
    public:
        joining_thread(std::thread& thd)
            : thd_{thd}
        {}

        joining_thread(const joining_thread&) = delete;
        joining_thread& operator=(const joining_thread&) = delete;
        joining_thread(joining_thread&&) = delete;
        joining_thread& operator=(joining_thread&&) = delete;

        ~joining_thread()
        {
            if (thd_.joinable())
                thd_.join();
        }

        std::thread& get()
        {
            return thd_;
        }
    };
}

int main() try
{
    std::cout << "Main thread starts..." << std::endl;

    std::thread thd1 = create_background_work();

    std::vector<ext::joining_thread> working_threads;

    working_threads.push_back(create_background_work());
    working_threads.push_back(create_background_work());
    working_threads.push_back(create_background_work());
    working_threads.push_back(std::move(thd1));

    working_threads.at(100) = create_background_work(); // throws an exception

    std::cout << "Main thread ends..." << std::endl;

    {
//        std::thread task1 = create_background_work();
//        Simplified::joining_thread jtask_1{task1};

        ext::joining_thread thd1(background_work, 1, "Hello", 200ms);
        ext::joining_thread thd2 = create_background_work();

        //Simplified::joining_thread jtask_2 = jtask_1;
    }
}
catch(...)
{
    std::cout << "Caught an exception..." << std::endl;
}
