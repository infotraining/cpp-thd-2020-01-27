#include <iostream>
#include <thread>
#include <mutex>

std::mutex mtx_cout;

class BankAccount
{
    const int id_;
    double balance_;
    mutable std::recursive_mutex mtx_;
public:
    BankAccount(int id, double balance)
        : id_(id)
        , balance_(balance)
    {
    }

    void print() const
    {
        std::lock_guard lk_cout{mtx_cout};
        std::cout << "Bank Account #" << id() << "; Balance = " << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {

#if __cplusplus > 201402L
        std::scoped_lock deadlock_free_lk{mtx_, to.mtx_};
#else
        std::unique_lock<std::mutex> lk_from(mtx_, std::defer_lock);
        std::unique_lock<std::mutex> lk_to(to.mtx_, std::defer_lock);

        std::lock(lk_from, lk_to);
#endif

//        balance_ -= amount;
//        to.balance_ += amount;

          withdraw(amount);
          to.deposit(amount);
    }

    void withdraw(double amount)
    {
        std::lock_guard<std::recursive_mutex> lk{mtx_}; // lock
        balance_ -= amount;
    } // unlock

    void deposit(double amount)
    {
        std::lock_guard lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<std::recursive_mutex> lk{mtx_};
        return balance_;
    }

    ///////////////////////////////////
    /// \brief External locking pattern
    ///
    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }

    bool try_lock()
    {
        return mtx_.try_lock();
    }

    auto with_lock()
    {
        return std::unique_lock<std::recursive_mutex>{mtx_};
    }
};

void make_withdraws(BankAccount& ba, int count, double amount)
{
    for(int i = 0; i < count; ++i)
        ba.withdraw(amount);
}

void make_deposits(BankAccount& ba, int count, double amount)
{
    for(int i = 0; i < count; ++i)
        ba.deposit(amount);
}

void make_transfer(BankAccount& from, BankAccount& to, int count, double amount)
{
    for(int i = 0; i < count; ++i)
        from.transfer(to, amount);
}

int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    std::thread thd_w{&make_withdraws, std::ref(ba1), 1'000'000, 1.0};
    std::thread thd_d{[&ba1] { make_deposits(ba1, 1'000'000, 1.0);}};
    std::thread thd_t1{[&ba1, &ba2] { make_transfer(ba1, ba2, 10000, 1.0);}};
    std::thread thd_t2{[&ba1, &ba2] { make_transfer(ba2, ba1, 10000, 1.0);}};

    {
        //std::lock_guard<BankAccount> lk_ba1{ba1};
        auto lk = ba1.with_lock();
        ba1.withdraw(100.0);
        ba1.deposit(50.0);
        ba1.transfer(ba2, 100.0);
    }

    thd_d.join();
    thd_w.join();
    thd_t1.join();
    thd_t2.join();

    std::cout << "\nAfter deposits & withdraws:\n";
    ba1.print();
    ba2.print();

//    std::thread prn1([&] {
//        for(int i = 0; i < 1000; ++i)
//        {
//            ba1.print();
//        }
//    });
//    std::thread prn2([&] {
//        for(int i = 0; i < 1000; ++i)
//        {
//            ba2.print();
//        }
//    });

//    prn1.join();
//    prn2.join();
}
