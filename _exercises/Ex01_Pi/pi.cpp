#include <atomic>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>
#include <cstdlib>
#include <ctime>
#include <random>
#include <new>
#include <mutex>
#include <future>

using namespace std;

namespace FalseSharing
{
    void calc_hits(long N, long& hits)
    {
        std::random_device rd;
        std::mt19937 rnd_gen(rd());
        std::uniform_real_distribution<> rnd_distr(0, 1.0);

        for (long n = 0; n < N; ++n)
        {
            double x = rnd_distr(rnd_gen);
            double y = rnd_distr(rnd_gen);

            if (x * x + y * y < 1)
                hits++;
        }
    }
}

namespace MemoryAlignment
{
    struct alignas(128) AlignedCounter
    {
        long value;
    };

    void calc_hits(long N, AlignedCounter& hits)
    {
        std::random_device rd;
        std::mt19937 rnd_gen(rd());
        std::uniform_real_distribution<> rnd_distr(0, 1.0);

        for (long n = 0; n < N; ++n)
        {
            double x = rnd_distr(rnd_gen);
            double y = rnd_distr(rnd_gen);

            if (x * x + y * y < 1)
                hits.value++;
        }
    }
}

namespace Atomic
{
    void calc_hits(long N, std::atomic<long>& hits)
    {
        std::random_device rd;
        std::mt19937 rnd_gen(rd());
        std::uniform_real_distribution<> rnd_distr(0, 1.0);

        for (long n = 0; n < N; ++n)
        {
            double x = rnd_distr(rnd_gen);
            double y = rnd_distr(rnd_gen);

            if (x * x + y * y < 1)
                hits.fetch_add(1, std::memory_order_relaxed);
        }
    }
}

void calc_hits(long N, long& hits)
{
    std::random_device rd;
    std::mt19937 rnd_gen(rd());
    std::uniform_real_distribution<> rnd_distr(0, 1.0);

    long local_hits = 0;

    for (long n = 0; n < N; ++n)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);

        if (x * x + y * y < 1)
            local_hits++;
    }

    hits = local_hits;
}

struct Hits
{
    long value = 0;
    std::mutex mtx;
};

void calc_hits_shared_state(long N, Hits& hits)
{
    std::random_device rd;
    std::mt19937 rnd_gen(rd());
    std::uniform_real_distribution<> rnd_distr(0, 1.0);

    long local_hits = 0;

    for (long n = 0; n < N; ++n)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);

        if (x * x + y * y < 1)
        {
            local_hits++;
        }
    }

#if __cplusplus > 201402L
    std::lock_guard lk{hits.mtx}; // since C++17
#else
    std::lock_guard<std::mutex> lk{hits.mtx};
#endif


    hits.value += local_hits;
}

long get_hits(long N)
{
    std::random_device rd;
    std::mt19937 rnd_gen(rd());
    std::uniform_real_distribution<> rnd_distr(0, 1.0);

    long local_hits = 0;

    for (long n = 0; n < N; ++n)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);

        if (x * x + y * y < 1)
            local_hits++;
    }

    return local_hits;
}


int main()
{
    const long N = 100'000'000;

    //////////////////////////////////////////////////////////////////////////////
    // single thread
    {
        long hits = 0;

        cout << "Pi calc (single threaded)!" << endl;
        const auto start = chrono::high_resolution_clock::now();

        calc_hits(N, hits);

        const auto end = chrono::high_resolution_clock::now();
        const auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

        cout << "Pi = " << double(hits) / double(N) * 4 << endl;
        cout << "Elapsed = " << elapsed_time << "ms" << endl;
    }

    //////////////////////////////////////////////////////////////////////////////
    {
        cout << "\nPi calc (multithreading)!" << endl;
        const auto start = chrono::high_resolution_clock::now();

        const auto threads_count = std::max(1u, std::thread::hardware_concurrency());
        const long N_per_thread = N / threads_count;

        std::vector<long> hits_per_thread(threads_count);
        std::vector<std::thread> threads;

        for(size_t i = 0; i < threads_count; ++i)
        {
            threads.emplace_back(&calc_hits, N_per_thread, std::ref(hits_per_thread[i]));
        }

        for(auto& thd : threads)
            thd.join();

        auto hits = std::accumulate(std::begin(hits_per_thread), std::end(hits_per_thread), 0L);

        const auto end = chrono::high_resolution_clock::now();
        const auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

        cout << "Pi = " << double(hits) / double(N) * 4 << endl;
        cout << "Elapsed = " << elapsed_time << "ms" << endl;                
    }

    //////////////////////////////////////////////////////////////////////////////
    {
        cout << "\nPi calc (multithreading - false sharing)!" << endl;
        const auto start = chrono::high_resolution_clock::now();

        const auto threads_count = std::max(1u, std::thread::hardware_concurrency());
        const long N_per_thread = N / threads_count;

        std::vector<long> hits_per_thread(threads_count);
        std::vector<std::thread> threads;

        for(size_t i = 0; i < threads_count; ++i)
        {
            threads.emplace_back(&FalseSharing::calc_hits, N_per_thread, std::ref(hits_per_thread[i]));
        }

        for(auto& thd : threads)
            thd.join();

        auto hits = std::accumulate(std::begin(hits_per_thread), std::end(hits_per_thread), 0L);

        const auto end = chrono::high_resolution_clock::now();
        const auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

        cout << "Pi = " << double(hits) / double(N) * 4 << endl;
        cout << "Elapsed = " << elapsed_time << "ms" << endl;
    }

    //////////////////////////////////////////////////////////////////////////////
    {
        cout << "\nPi calc (multithreading - memory aligned for cache line)!" << endl;
        const auto start = chrono::high_resolution_clock::now();

        const auto threads_count = std::max(1u, std::thread::hardware_concurrency());
        const long N_per_thread = N / threads_count;

        std::vector<MemoryAlignment::AlignedCounter> hits_per_thread(threads_count);
        std::vector<std::thread> threads;

        for(size_t i = 0; i < threads_count; ++i)
        {
            threads.emplace_back(&MemoryAlignment::calc_hits, N_per_thread, std::ref(hits_per_thread[i]));
        }

        for(auto& thd : threads)
            thd.join();

        auto hits = std::accumulate(std::begin(hits_per_thread), std::end(hits_per_thread), 0L,
            [](long a, const MemoryAlignment::AlignedCounter& b) { return a + b.value;});

        const auto end = chrono::high_resolution_clock::now();
        const auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

        cout << "Pi = " << double(hits) / double(N) * 4 << endl;
        cout << "Elapsed = " << elapsed_time << "ms" << endl;
    }

    //////////////////////////////////////////////////////////////////////////////
    {
        cout << "\nPi calc (mutex)!" << endl;
        const auto start = chrono::high_resolution_clock::now();

        const auto threads_count = std::max(1u, std::thread::hardware_concurrency());
        const long N_per_thread = N / threads_count;


        std::vector<std::thread> threads;
        Hits hits;

        for(size_t i = 0; i < threads_count; ++i)
        {
            threads.emplace_back(&calc_hits_shared_state, N_per_thread, std::ref(hits));
        }

        for(auto& thd : threads)
            thd.join();

        const auto end = chrono::high_resolution_clock::now();
        const auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

        cout << "Pi = " << double(hits.value) / double(N) * 4 << endl;
        cout << "Elapsed = " << elapsed_time << "ms" << endl;
    }

    //////////////////////////////////////////////////////////////////////////////
    {
        cout << "\nPi calc (atomic)!" << endl;
        const auto start = chrono::high_resolution_clock::now();

        const auto threads_count = std::max(1u, std::thread::hardware_concurrency());
        const long N_per_thread = N / threads_count;


        std::vector<std::thread> threads;
        std::atomic<long> hits{0};

        for(size_t i = 0; i < threads_count; ++i)
        {
            threads.emplace_back(&Atomic::calc_hits, N_per_thread, std::ref(hits));
        }

        for(auto& thd : threads)
            thd.join();

        const auto end = chrono::high_resolution_clock::now();
        const auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

        cout << "Pi = " << double(hits) / double(N) * 4 << endl;
        cout << "Elapsed = " << elapsed_time << "ms" << endl;
    }

    //////////////////////////////////////////////////////////////////////////////
    {
        cout << "\nPi calc (atomic)!" << endl;
        const auto start = chrono::high_resolution_clock::now();

        const auto threads_count = std::max(1u, std::thread::hardware_concurrency());
        const long N_per_thread = N / threads_count;

        std::vector<std::future<long>> f_hits;

        for(size_t i = 0; i < threads_count; ++i)
            f_hits.push_back(std::async(std::launch::async, &get_hits, N_per_thread));

        long hits{};
        for(auto& f : f_hits)
            hits += f.get();

        const auto end = chrono::high_resolution_clock::now();
        const auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

        cout << "Pi = " << double(hits) / double(N) * 4 << endl;
        cout << "Elapsed = " << elapsed_time << "ms" << endl;
    }
}

