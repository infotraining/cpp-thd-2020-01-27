#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <map>
#include "joining_thread.hpp"

using namespace std::literals;

const std::map<int, std::string> dict = { {1, "one"}, {2, "two"}, {42, "THE ANWSER"}, {665, "lesser evil"} };

struct LookupResult
{
    std::string value;
    std::exception_ptr eptr;
};

void do_lookup(const std::map<int, std::string>& dict, int key, LookupResult& result)
{
    try
    {
        std::cout << "Thread#" << std::this_thread::get_id() << " is looking for " << key << "\n";

        std::this_thread::sleep_for(std::chrono::milliseconds(key * 50));

        result.value = dict.at(key);

        std::cout << "Thread#" << std::this_thread::get_id() << " is finished..." << std::endl;
    }
    catch(...)
    {
        std::cout << "Caught an exception in thread#" << std::this_thread::get_id() << std::endl;
        result.eptr = std::current_exception();
    }
}

int main() try
{
    std::cout << "Main thread starts..." << std::endl;

    std::vector<int> keys = {1, 2, 42, 65, 3};
    std::vector<LookupResult> results(keys.size());

    {
        std::vector<ext::joining_thread> threads;

        for(size_t i = 0; i < keys.size(); ++i)
        {
            threads.push_back(
                ext::joining_thread(&do_lookup, std::cref(dict), keys[i], std::ref(results[i])));
        }
    } // implicit join

    for(auto& result : results)
    {
        if (result.eptr)
        {
            try
            {
                std::rethrow_exception(result.eptr);
            }
            catch (const std::out_of_range& e)
            {
                std::cout << "Caught: " << e.what() << std::endl;
            }
            catch(const std::exception& e)
            {
                std::cout << "General exception case" << std::endl;
            }
        }
        else
        {
            std::cout << "result : " << result.value << "\n";
        }
    }

    std::cout << "Main thread ends..." << std::endl;
}
catch(...)
{
    std::cout << "Caught an exception...\n";
}
