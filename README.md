# Multithreading in C++

## Docs

* https://infotraining.bitbucket.io/cpp-thd/
* https://gitpitch.com/infotraining-dev/cpp-thd-slides/master?grs=bitbucket

## Virtual Box

### login and password for VM:

```
dev  /  pwd
```

### reinstall VBox addon (optional)

```
sudo /etc/init.d/vboxadd setup
```

### proxy settings (optional)

We can add them to `.profile`

```
export http_proxy=http://aaa.bbb.ccc.ddd:port
export https_proxy=https://aaa.bbb.ccc.ddd:port
```

### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

## Source repository

```
git clone https://infotraining-dev@bitbucket.org/infotraining/cpp-thd-2020-01-27.git
```

## Links

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

* [Codinghorror - Infinite space between words](https://blog.codinghorror.com/the-infinite-space-between-words/)

* [compiler explorer](https://gcc.godbolt.org/)

* [Atomics to CPU mappings](https://www.cl.cam.ac.uk/~pes20/cpp/cpp0xmappings.html)

* [Atomic weapons 1](https://www.youtube.com/watch?v=A8eCGOqgvH4)

* [Atomic weapons 2](https://www.youtube.com/watch?v=KeLBd2EJLOU)

* [Dan Saks - C++ for C programmers](https://www.youtube.com/watch?v=D7Sd8A6_fYU&t=51s)

## ANKIETA

* https://www.infotraining.pl/ankieta/cpp-thd-2020-01-27-kp
