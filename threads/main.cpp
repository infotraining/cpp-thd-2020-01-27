#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

class BackgroundWork
{
    const int id_;
    const std::string text_;

public:
    BackgroundWork(int id, std::string text)
        : id_{id}, text_{std::move(text)}
    {}

    void operator()(std::chrono::milliseconds delay) const
    {
        std::cout << "BW#" << id_ << " has started..." << std::endl;

        for(const auto& c : text_)
        {
            std::cout << "BW#" << id_ << ": " << c << std::endl;
            std::this_thread::sleep_for(delay);
        }

        std::cout << "BW#" << id_ << " is finished..." << std::endl;
    }
};


int main()
{
    std::cout << "Main thread starts..." << std::endl;

    std::thread thd_empty; // NOT-A-THREAD
    std::cout << thd_empty.get_id() << std::endl;

    const std::string text = "Hello threads";

    std::thread thd1(&background_work, 1, std::cref(text) , 250ms);
    std::thread thd2(BackgroundWork{2, "function object"}, 150ms);
    std::thread thd3([&text] { background_work(3, text, 75ms); } );
    std::thread thd4([] { background_work(4, "DEAMON", 1s); });

    background_work(0, "MAIN THREAD", 100ms);

    std::this_thread::sleep_for(5s);

    std::thread thd5 = std::move(thd1);

    thd2.join();
    thd5.join();
    thd3.join();
    thd4.detach();

    assert(!thd3.joinable());
    assert(!thd4.joinable());

    std::cout << "\n\n";

    const std::vector<int> source = {1, 2, 3, 4, 5, 6, 7 };

    std::vector<int> target;
    std::vector<int> backup;

    std::thread thd_copy([&] { std::copy(begin(source), end(source), std::back_inserter(target)); });
    std::thread thd_backup([&] { std::copy(begin(source), end(source), std::back_inserter(backup)); });

    thd_copy.join();

    std::cout << "target: ";
    for(const auto& item : target)
        std::cout << item << " ";
    std::cout << std::endl;


    thd_backup.join();

    std::cout << "backup: ";
    for(const auto& item : backup)
        std::cout << item << " ";
    std::cout << std::endl;

    std::cout << "Main thread ends..." << std::endl;

    auto hardware_threads_count =
        std::max(1u, std::thread::hardware_concurrency());

    std::cout << "hardware concurrency: " << hardware_threads_count << "\n";
    std::cout << "Main thread id: " << std::this_thread::get_id() << "\n";
}
