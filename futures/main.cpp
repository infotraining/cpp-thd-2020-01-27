#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <random>
#include <future>
#include "joining_thread.hpp"

using namespace std::literals;

int calculate_square(int x)
{
    std::cout << "Starting calculations for " << x << std::endl;

    if (x % 7 == 0)
        throw std::runtime_error("ERROR#7");

    std::random_device rd;
    std::uniform_int_distribution<> rnd_gen(200, 2000);
    std::this_thread::sleep_for(std::chrono::milliseconds(rnd_gen(rd)));

    return x * x;
}

void save_to_file(const std::string& filename)
{
    std::cout << "Saving to file: " << filename << std::endl;

    std::random_device rd;
    std::uniform_int_distribution<> rnd_gen(1000, 5000);
    std::this_thread::sleep_for(std::chrono::milliseconds(rnd_gen(rd)));

    std::cout << "File saved: " << filename << std::endl;
}

void futures_with_packaged_tasks()
{
    std::packaged_task<int()> pt1{[] { return calculate_square(7); }};
    std::packaged_task<int(int)> pt2{&calculate_square};
    std::packaged_task<void()> pt3{[] { save_to_file("data1.txt");}};

    std::future<int> f1 = pt1.get_future();
    std::future<int> f2 = pt2.get_future();
    std::future<void> f3 = pt3.get_future();

    std::thread thd1{std::move(pt1)};
    ext::joining_thread thd2{std::move(pt2), 9};
    std::thread thd3{std::move(pt3)};
    thd3.detach();

    try
    {
        auto result1 = f1.get();
        std::cout << "Result 1: " << result1 << std::endl;
    }
    catch(const std::runtime_error& e)
    {
        std::cout << "Caught an exception: " << e.what() << std::endl;
    }

    std::cout << "Result 2: " << f2.get() << std::endl;

    while (f3.wait_for(100ms) != std::future_status::ready)
    {
        std::cout << ".";
        std::cout.flush();
    }

    thd1.join();
}

class SquareCalculator
{
    std::promise<int> promise_;
public:
    void calculate(int x)
    {
        try
        {
            int result = calculate_square(x);
            promise_.set_value(result);
        }
        catch(...)
        {
            promise_.set_exception(std::current_exception());
        }
    }

    std::future<int> get_future()
    {
        return promise_.get_future();
    }
};

void futures_with_promise()
{
    SquareCalculator calculator;

    std::future<int> f = calculator.get_future();

    ext::joining_thread calc_thread{[&calculator] { calculator.calculate(7);} };

    try
    {
        std::cout << "Result: " << f.get() << std::endl;
    }
    catch(const std::runtime_error& e)
    {
        std::cout << e.what() << std::endl;
    }
}

void future_with_async()
{
    std::future<int> f1 = std::async(std::launch::async, &calculate_square, 13);
    std::future<int> f2 = std::async(std::launch::deferred, &calculate_square, 6);
    std::future<int> f3 = std::async(std::launch::async, &calculate_square, 22);

    std::vector<std::future<int>> f_results;
    f_results.push_back(std::move(f1));
    f_results.push_back(std::move(f2));
    f_results.push_back(std::move(f3));

    for(int i = 1; i < 10; ++i)
        f_results.push_back(std::async(std::launch::async, &calculate_square, i));

    std::this_thread::sleep_for(10s);

    for(auto& f : f_results)
    {
        try
        {
            std::cout << f.get() << "\n";
        }
        catch(const std::runtime_error& e)
        {
            std::cout << e.what() << std::endl;
        }
    }
}

void wtf()
{
    auto f1 = std::async(std::launch::async, &save_to_file, "file1.dat");
    auto f2 = std::async(std::launch::async, &save_to_file, "file2.dat");
    auto f3 = std::async(std::launch::async, &save_to_file, "file3.dat");
    auto f4 = std::async(std::launch::async, &save_to_file, "file4.dat");
    auto f5 = std::async(std::launch::async, &save_to_file, "file5.dat");
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    //futures_with_packaged_tasks();
    //futures_with_promise();
    //future_with_async();

    wtf();

    std::cout << "Main thread ends..." << std::endl;
}
