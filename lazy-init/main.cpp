#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>
#include "joining_thread.hpp"

using namespace std::literals;

class Service
{
public:
    virtual void run() = 0;
    virtual ~Service() = default;
};

class RealService : public Service
{
    std::string url_;
public:
    RealService(std::string url) : url_{std::move(url)}
    {
        std::cout << "Creating a real service..." << std::endl;
        std::this_thread::sleep_for(1s);
        std::cout << "Openning connection..." << std::endl;
        std::this_thread::sleep_for(2s);
    }

    void run() override
    {
        std::cout << "RealService is running..." << std:: endl;
    }
};

namespace Atomic
{
class ProxyService : public Service
{
    std::mutex mtx_srv_;
    std::atomic<RealService*> srv_{nullptr};
    std::string url_;

public:
    ProxyService(std::string url) : url_{std::move(url)}
    {
        std::cout << "Proxy service created..." << std::endl;
    }

    ProxyService(const ProxyService&) = delete;
    ProxyService& operator=(const ProxyService&) = delete;

    ~ProxyService() override
    {
        delete srv_;
    }

    void run() override
    {
        {
            if (srv_ == nullptr) // if (srv_.load() == nullptr)
            {
                std::lock_guard<std::mutex> lk{mtx_srv_};

                if (srv_ == nullptr)
                {
                    auto ptr = new RealService(url_);
                    srv_ = ptr; // srv_.store(ptr);

//                    //1
//                    void* raw_mem = ::operator new(sizeof(RealService));

//                    //2
//                    new (raw_mem) RealService(url_);

//                    //3
//                    srv_.store(static_cast<RealService*>(raw_mem)); /////// BARRIER PREVENTS REORDERING
                }
            }
        }

        srv_.load()->run();
    }
};
}

class ProxyService : public Service
{
    std::unique_ptr<RealService> srv_{nullptr};
    std::string url_;
    std::once_flag init_flag_;
public:
    ProxyService(std::string url) : url_{std::move(url)}
    {
        std::cout << "Proxy service created..." << std::endl;
    }

    void run() override
    {
        std::call_once(init_flag_, [this] { srv_ = std::make_unique<RealService>(url_);});

        srv_->run();
    }
};

void client(Service& srv)
{
    srv.run();
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    ProxyService srv("https://service.com");

    {
        ext::joining_thread thd1{[&] { client(srv);} };
        ext::joining_thread thd2{[&] { client(srv);} };
    }

    std::cout << "Main thread ends..." << std::endl;
}
