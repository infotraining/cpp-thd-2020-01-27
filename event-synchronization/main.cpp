#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <random>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include "joining_thread.hpp"

using namespace std::literals;

namespace Atomic
{
class Data
{
    std::vector<int> data_;
    std::atomic<bool> is_ready_{false};
public:
    void read()
    {
        std::cout << "Start reading..." << std::endl;
        std::this_thread::sleep_for(2s);

        std::random_device rd;
        std::mt19937 rnd_gen(rd());
        std::uniform_int_distribution<> rnd_distr(0, 1000);

        data_.resize(100);
        std::generate(begin(data_), end(data_), [&] { return rnd_distr(rnd_gen);});

        is_ready_ = true;
        //is_ready_.store(true, std::memory_order::memory_order_release); /////////////////////////////// RELEASE
    }

    void process(int id)
    {
        //while (!is_ready_.load(), std::memory_order::memory_order_acquire) //////////////////////////// ACQUIRE
        while(!is_ready_)
        {
        }

        auto sum = std::accumulate(begin(data_), end(data_), 0L);

        std::cout << "Id: " << id << "; Sum: " << sum << std::endl;
    }
};
}

namespace IdleWaits
{
    class Data
    {
        std::vector<int> data_;
        bool is_ready_ = false;
        std::mutex mtx_is_ready_;
        std::condition_variable cv_is_ready_;
    public:
        void read()
        {
            std::cout << "Start reading..." << std::endl;
            std::this_thread::sleep_for(2s);

            std::random_device rd;
            std::mt19937 rnd_gen(rd());
            std::uniform_int_distribution<> rnd_distr(0, 1000);

            data_.resize(100);
            std::generate(begin(data_), end(data_), [&] { return rnd_distr(rnd_gen);});

            {
                std::lock_guard<std::mutex> lk{mtx_is_ready_};
                is_ready_ = true;
            }

            cv_is_ready_.notify_all();
        }

        void process(int id)
        {
            std::unique_lock<std::mutex> lk{mtx_is_ready_};
            cv_is_ready_.wait(lk, [this]{ return is_ready_; });

            lk.unlock();

            auto sum = std::accumulate(begin(data_), end(data_), 0L);

            std::cout << "Id: " << id << "; Sum: " << sum << std::endl;
        }
    };
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    IdleWaits::Data data;

    {
        ext::joining_thread thd_producer{[&data] { data.read(); } };

        ext::joining_thread thd_consumer_1{[&data] { data.process(1);}};
        ext::joining_thread thd_consumer_2{[&data] { data.process(2);}};
    }

    std::cout << "Main thread ends..." << std::endl;
}
